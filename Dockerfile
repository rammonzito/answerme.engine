FROM microsoft/dotnet:2.1-aspnetcore-runtime AS base
WORKDIR /app
EXPOSE 5000
ENV ASPNETCORE_URLS=http://+:5000

FROM microsoft/dotnet:2.1-sdk AS build
WORKDIR /src

COPY ["AnswerMe.API/AnswerMe.API/AnswerMe.API.csproj", "AnswerMe.API/"]
COPY ["AnswerMe.API/AnswerMe.Application/AnswerMe.Application.csproj", "AnswerMe.Application/"]
COPY ["AnswerMe.API/AnswerMe.Application.DTO/AnswerMe.Application.DTO.csproj", "AnswerMe.Application.DTO/"]
COPY ["AnswerMe.API/AnswerMe.Domain/AnswerMe.Domain.csproj", "AnswerMe.Domain/"]
COPY ["AnswerMe.API/AnswerMe.Domain.Core/AnswerMe.Domain.Core.csproj", "AnswerMe.Domain.Core/"]
COPY ["AnswerMe.API/AnswerMe.Domain.Services/AnswerMe.Domain.Services.csproj", "AnswerMe.Domain.Services/"]
COPY ["AnswerMe.API/AnswerMe.Infrastructure.CrossCutting.Adapter/AnswerMe.Infrastructure.CrossCutting.Adapter.csproj", "AnswerMe.Infrastructure.CrossCutting.Adapter/"]
COPY ["AnswerMe.API/AnswerMe.Infrastructure.Data/AnswerMe.Infrastructure.Data.csproj", "AnswerMe.Infrastructure.Data/"]
COPY ["AnswerMe.API/AnswerMe.Infrastructure.IoC/AnswerMe.Infrastructure.CrossCutting.IoC.csproj", "AnswerMe.Infrastructure.IoC/"]
COPY ["AnswerMe.API/AnswerMe.Infrastructure.Repository/AnswerMe.Infrastructure.Repository.csproj", "AnswerMe.Infrastructure.Repository/"]
COPY ["AnswerMe.API/AnswerMeAPI.API.Test/AnswerMeAPI.API.Test.csproj", "AnswerMeAPI.API.Test/"]

RUN dotnet restore "AnswerMe.API/AnswerMe.API.csproj"
COPY . .
WORKDIR "/src/AnswerMe.API"
RUN dotnet build "AnswerMe.API/AnswerMe.API.csproj" -c Release -o /app

FROM build AS publish
RUN dotnet publish "AnswerMe.API/AnswerMe.API.csproj" -c Release -o /app

FROM base AS final
WORKDIR /app
COPY --from=publish /app .
ENTRYPOINT ["dotnet", "AnswerMe.API.dll", "--server.urls", "http://0.0.0.0:5000"]