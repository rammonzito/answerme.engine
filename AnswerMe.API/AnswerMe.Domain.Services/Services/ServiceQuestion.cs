﻿
using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;

namespace AnswerMe.Domain.Core.Services.Services
{
    public class ServiceQuestion : ServiceBase<Question>, IServiceQuestion
    {
        private readonly IRepositoryQuestion _repositoryQuestion;

        public ServiceQuestion(IRepositoryQuestion repository)
            : base(repository)
        {
            _repositoryQuestion = repository;
        }
    }
}
