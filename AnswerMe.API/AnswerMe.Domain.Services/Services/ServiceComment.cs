﻿using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;


namespace AnswerMe.Domain.Core.Services.Services
{
    public class ServiceComment : ServiceBase<Comment>, IServiceComment
    {
        private readonly IRepositoryComment _repositoryComment;

        public ServiceComment(IRepositoryComment repository)
            : base(repository)
        {
            _repositoryComment = repository;
        }
    }
}
