﻿using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;

namespace AnswerMe.Domain.Core.Services.Services
{
    public class ServiceAnswer : ServiceBase<Answer>, IServiceAnswer
    {
        private readonly IRepositoryAnswer _repositoryAnswer;

        public ServiceAnswer(IRepositoryAnswer repository)
            : base(repository)
        {
            _repositoryAnswer = repository;
        }
    }
}
