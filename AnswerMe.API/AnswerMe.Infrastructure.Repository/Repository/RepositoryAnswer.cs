﻿using AnswerMe.Domain.Core.Interfaces;
using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.Data;

namespace AnswerMe.Infrastructure.Repository.Repository
{
    public class RepositoryAnswer : RepositoryBase<Answer>, IRepositoryAnswer
    {
        private readonly SqlContext _context;

        public RepositoryAnswer(SqlContext context) 
            : base(context)
        {
            _context = context;
        }
    }
}
