﻿using AnswerMe.Domain.Core.Interfaces;
using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.Data;

namespace AnswerMe.Infrastructure.Repository.Repository
{
    public class RepositoryQuestion : RepositoryBase<Question>, IRepositoryQuestion
    {
        private readonly SqlContext _context;
        public RepositoryQuestion(SqlContext context) : base(context)
        {
            _context = context;
        }
    }
}
