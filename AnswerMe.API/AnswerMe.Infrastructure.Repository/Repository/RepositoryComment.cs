﻿using AnswerMe.Domain.Core.Interfaces;
using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.Data;

namespace AnswerMe.Infrastructure.Repository.Repository
{
    public class RepositoryComment : RepositoryBase<Comment>, IRepositoryComment
    {
        private readonly SqlContext _context;
        public RepositoryComment(SqlContext context) : base(context)
        {
            _context = context;
        }
    }
}
