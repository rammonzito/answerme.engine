﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperComment
    {
        Comment MapperToEntity(CommentDTO commentDTO);
        IEnumerable<CommentDTO> MapperListComments(IEnumerable<Comment> comments);
        CommentDTO MapperToDTO(Comment comment);
    }
}
