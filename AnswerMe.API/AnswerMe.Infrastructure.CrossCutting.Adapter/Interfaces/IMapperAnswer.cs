﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperAnswer
    {
        Answer MapperToEntity(AnswerDTO answerDTO);
        IEnumerable<AnswerDTO> MapperListAnswers(IEnumerable<Answer> answers);
        AnswerDTO MapperToDTO(Answer answer);
    }
}
