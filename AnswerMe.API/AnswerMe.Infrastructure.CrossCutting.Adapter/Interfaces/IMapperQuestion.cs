﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces
{
    public interface IMapperQuestion
    {
        Question MapperToEntity(QuestionDTO questionsDTO);
        IEnumerable<QuestionDTO> MapperListQuestions(IEnumerable<Question> questions);
        QuestionDTO MapperToDTO(Question question);
    }
}
