﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Map
{
    public class MapperQuestion : IMapperQuestion
    {
        List<QuestionDTO> QuestionsDTO = new List<QuestionDTO>();

        public IEnumerable<QuestionDTO> MapperListQuestions(IEnumerable<Question> Questions)
        {
            foreach (var item in Questions)
            {
                QuestionDTO QuestionDTO = new QuestionDTO
                {
                    Id = item.Id,
                    Description = item.Description
                };
                QuestionsDTO.Add(QuestionDTO);
            }

            return QuestionsDTO;
        }

        public QuestionDTO MapperToDTO(Question Question)
        {
            QuestionDTO QuestionDTO = new QuestionDTO
            {
                Id = Question.Id,
                Description = Question.Description
            };

            return QuestionDTO;
        }

        public Question MapperToEntity(QuestionDTO QuestionDTO)
        {
            Question Question = new Question
            {
                Id = QuestionDTO.Id,
                Description = QuestionDTO.Description
            };

            return Question;
        }
    }
}
