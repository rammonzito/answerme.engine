﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Map
{
    public class MapperAnswer : IMapperAnswer
    {
        List<AnswerDTO> answersDTO = new List<AnswerDTO>();

        public IEnumerable<AnswerDTO> MapperListAnswers(IEnumerable<Answer> answers)
        {
            foreach (var item in answers)
            {
                AnswerDTO answerDTO = new AnswerDTO
                {
                    Id = item.Id,
                    Correct = item.Correct,
                    Description = item.Description
                };
                answersDTO.Add(answerDTO);
            }

            return answersDTO;
        }

        public AnswerDTO MapperToDTO(Answer answer)
        {
            AnswerDTO answerDTO = new AnswerDTO
            {
                Id = answer.Id,
                Correct = answer.Correct,
                Description = answer.Description
            };

            return answerDTO;
        }

        public Answer MapperToEntity(AnswerDTO answerDTO)
        {
            Answer answer = new Answer
            {
                Id = answerDTO.Id,
                Description = answerDTO.Description,
                Correct = answerDTO.Correct
            };

            return answer;
        }
    }
}
