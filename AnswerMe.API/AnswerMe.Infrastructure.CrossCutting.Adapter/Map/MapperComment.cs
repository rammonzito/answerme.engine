﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Domain.Models;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace AnswerMe.Infrastructure.CrossCutting.Adapter.Map
{
    public class MapperComment : IMapperComment
    {
        List<CommentDTO> CommentsDTO = new List<CommentDTO>();

        public IEnumerable<CommentDTO> MapperListComments(IEnumerable<Comment> Comments)
        {
            foreach (var item in Comments)
            {
                CommentDTO CommentDTO = new CommentDTO
                {
                    Id = item.Id,
                    Description = item.Description
                };
                CommentsDTO.Add(CommentDTO);
            }

            return CommentsDTO;
        }

        public CommentDTO MapperToDTO(Comment Comment)
        {
            CommentDTO CommentDTO = new CommentDTO
            {
                Id = Comment.Id,
                Description = Comment.Description
            };

            return CommentDTO;
        }

        public Comment MapperToEntity(CommentDTO CommentDTO)
        {
            Comment Comment = new Comment
            {
                Id = CommentDTO.Id,
                Description = CommentDTO.Description
            };

            return Comment;
        }
}
}
