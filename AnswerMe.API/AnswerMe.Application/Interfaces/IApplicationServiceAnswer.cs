﻿using AnswerMe.Application.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.Interfaces
{
    public interface IApplicationServiceAnswer
    {
        void Add(AnswerDTO obj);
        AnswerDTO GetById(int id);
        IEnumerable<AnswerDTO> GetAll();
        void Update(AnswerDTO obj);
        void Remove(AnswerDTO obj);
        void Dispose();
    }
}
