﻿using AnswerMe.Application.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.Interfaces
{
    public interface IApplicationServiceComment
    {
        void Add(CommentDTO obj);
        CommentDTO GetById(int id);
        IEnumerable<CommentDTO> GetAll();
        void Update(CommentDTO obj);
        void Remove(CommentDTO obj);
        void Dispose();
    }
}
