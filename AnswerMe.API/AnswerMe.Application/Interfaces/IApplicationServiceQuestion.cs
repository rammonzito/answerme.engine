﻿using AnswerMe.Application.DTO.DTO;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.Interfaces
{
    public interface IApplicationServiceQuestion
    {
        void Add(QuestionDTO obj);
        QuestionDTO GetById(int id);
        IEnumerable<QuestionDTO> GetAll();
        void Update(QuestionDTO obj);
        void Remove(QuestionDTO obj);
        void Dispose();
    }
}
