﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Application.Interfaces;
using AnswerMe.Domain.Core.Services;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace AnswerMe.Application.Services
{
    public class ApplicationServiceComment : IApplicationServiceComment
    {
        private readonly IServiceComment _servicesComment;
        private readonly IMapperComment _mapperComment;

        public ApplicationServiceComment(IServiceComment serviceComment, IMapperComment mapperComment)
        {
            _servicesComment = serviceComment;
            _mapperComment = mapperComment;
        }

        public void Add(CommentDTO obj)
        {
            var objComment = _mapperComment.MapperToEntity(obj);
            _servicesComment.Add(objComment);
        }

        public void Dispose()
        {
            _servicesComment.Dispose();
        }

        public IEnumerable<CommentDTO> GetAll()
        {
            var objComments = _servicesComment.GetAll();
            return _mapperComment.MapperListComments(objComments);
        }

        public CommentDTO GetById(int id)
        {
            var objComment = _servicesComment.GetById(id);
            return _mapperComment.MapperToDTO(objComment);
        }

        public void Remove(CommentDTO obj)
        {
            var objComment = _mapperComment.MapperToEntity(obj);
            _servicesComment.Remove(objComment);
        }

        public void Update(CommentDTO obj)
        {
            var objComment = _mapperComment.MapperToEntity(obj);
            _servicesComment.Update(objComment);
        }
    }
}
