﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Application.Interfaces;
using AnswerMe.Domain.Core.Services;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.Services
{
    public class ApplicationServiceAnswer : IApplicationServiceAnswer
    {
        private readonly IServiceAnswer _servicesAnswer;
        private readonly IMapperAnswer _mapperAnswer;

        public ApplicationServiceAnswer(IServiceAnswer serviceAnswer, IMapperAnswer mapperAnswer)
        {
            _servicesAnswer = serviceAnswer;
            _mapperAnswer = mapperAnswer;
        }

        public void Add(AnswerDTO obj)
        {
            var objAnswer = _mapperAnswer.MapperToEntity(obj);
            _servicesAnswer.Add(objAnswer);
        }

        public void Dispose()
        {
            _servicesAnswer.Dispose();
        }

        public IEnumerable<AnswerDTO> GetAll()
        {
            var objAnswers = _servicesAnswer.GetAll();
            return _mapperAnswer.MapperListAnswers(objAnswers);
        }

        public AnswerDTO GetById(int id)
        {
            var objAnswer = _servicesAnswer.GetById(id);
            return _mapperAnswer.MapperToDTO(objAnswer);
        }

        public void Remove(AnswerDTO obj)
        {
            var objAnswer = _mapperAnswer.MapperToEntity(obj);
            _servicesAnswer.Remove(objAnswer);
        }

        public void Update(AnswerDTO obj)
        {
            var objAnswer = _mapperAnswer.MapperToEntity(obj);
            _servicesAnswer.Update(objAnswer);
        }
    }
}
