﻿using AnswerMe.Application.DTO.DTO;
using AnswerMe.Application.Interfaces;
using AnswerMe.Domain.Core.Services;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using System.Collections.Generic;

namespace AnswerMe.Application.Services
{
    public class ApplicationServiceQuestion : IApplicationServiceQuestion
    {
        private readonly IServiceQuestion _servicesQuestion;
        private readonly IMapperQuestion _mapperQuestion;

        public ApplicationServiceQuestion(IServiceQuestion serviceQuestion, IMapperQuestion mapperQuestion)
        {
            _servicesQuestion = serviceQuestion;
            _mapperQuestion = mapperQuestion;
        }

        public void Add(QuestionDTO obj)
        {
            var objQuestion = _mapperQuestion.MapperToEntity(obj);
            _servicesQuestion.Add(objQuestion);
        }

        public void Dispose()
        {
            _servicesQuestion.Dispose();
        }

        public IEnumerable<QuestionDTO> GetAll()
        {
            var objQuestions = _servicesQuestion.GetAll();
            return _mapperQuestion.MapperListQuestions(objQuestions);
        }

        public QuestionDTO GetById(int id)
        {
            var objQuestion = _servicesQuestion.GetById(id);
            return _mapperQuestion.MapperToDTO(objQuestion);
        }

        public void Remove(QuestionDTO obj)
        {
            var objQuestion = _mapperQuestion.MapperToEntity(obj);
            _servicesQuestion.Remove(objQuestion);
        }

        public void Update(QuestionDTO obj)
        {
            var objQuestion = _mapperQuestion.MapperToEntity(obj);
            _servicesQuestion.Update(objQuestion);
        }
    }
}
