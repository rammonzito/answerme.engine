﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Domain.Models
{
    public class Base
    {
        public int Id { get; set; }
        public string Description { get; set; }
    }
}
