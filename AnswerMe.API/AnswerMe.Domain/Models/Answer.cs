﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Domain.Models
{
    public class Answer : Base
    {
        public bool Correct { get; set; }
    }
}
