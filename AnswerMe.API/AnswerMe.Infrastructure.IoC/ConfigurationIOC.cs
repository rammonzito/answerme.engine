﻿using AnswerMe.Application.Interfaces;
using AnswerMe.Application.Services;
using AnswerMe.Domain.Core.Interfaces.Repository;
using AnswerMe.Domain.Core.Services;
using AnswerMe.Domain.Core.Services.Services;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Interfaces;
using AnswerMe.Infrastructure.CrossCutting.Adapter.Map;
using AnswerMe.Infrastructure.Repository.Repository;
using Autofac;

namespace AnswerMe.Infrastructure.CrossCutting.IoC
{
    public class ConfigurationIOC
    {
        public static void Load(ContainerBuilder builder)
        {
            //IoC Application
            builder.RegisterType<ApplicationServiceAnswer>().As<IApplicationServiceAnswer>();
            builder.RegisterType<ApplicationServiceQuestion>().As<IApplicationServiceQuestion>();
            builder.RegisterType<ApplicationServiceComment>().As<IApplicationServiceComment>();

            //IoC Service
            builder.RegisterType<ServiceAnswer>().As<IServiceAnswer>();
            builder.RegisterType<ServiceQuestion>().As<IServiceQuestion>();
            builder.RegisterType<ServiceComment>().As<IServiceComment>();
                
            //IoC Sql
            builder.RegisterType<RepositoryAnswer>().As<IRepositoryAnswer>();
            builder.RegisterType<RepositoryQuestion>().As<IRepositoryQuestion>();
            builder.RegisterType<RepositoryComment>().As<IRepositoryComment>();


            //Mapper
            builder.RegisterType<MapperAnswer>().As<IMapperAnswer>();
            builder.RegisterType<MapperQuestion>().As<IMapperQuestion>();
            builder.RegisterType<MapperComment>().As<IMapperComment>();

        }
    }
}
