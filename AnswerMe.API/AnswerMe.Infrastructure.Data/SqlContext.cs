﻿using AnswerMe.Domain.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Linq;

namespace AnswerMe.Infrastructure.Data
{
    public class SqlContext : DbContext
    {
        public SqlContext()
        {

        }

        public SqlContext(DbContextOptions<SqlContext> options) : base(options) { }

        public DbSet<Answer> Answers { get; set; }
        public DbSet<Question> Questions { get; set; }
        public DbSet<Comment> Comments { get; set; }

        public override int SaveChanges()
        {
            foreach (var entry in ChangeTracker.Entries().Where(entry => entry.Entity.GetType().GetProperty("Created") != null))
            {
                if (entry.State == EntityState.Added)
                    entry.Property("Created").CurrentValue = DateTime.Now;
                if (entry.State == EntityState.Modified)
                    entry.Property("Created").IsModified = false;
            }
            return base.SaveChanges();
        }
    }
}
