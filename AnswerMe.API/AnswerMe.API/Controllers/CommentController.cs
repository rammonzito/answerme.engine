﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AnswerMe.Application.Interfaces;
using AnswerMe.Application.DTO.DTO;

namespace AnswerMe.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CommentController : ControllerBase
    {
        private readonly IApplicationServiceComment _applicationServiceComment;

        public CommentController(IApplicationServiceComment applicationServiceComment)
        {
            _applicationServiceComment = applicationServiceComment;
        }

        // GET: api/Comment
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceComment.GetAll());
        }

        // GET: api/Comment/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceComment.GetById(id));
        }

        // POST: api/Comment
        [HttpPost]
        public ActionResult Post([FromBody] CommentDTO CommentDTO)
        {
            try
            {
                if (CommentDTO == null)
                    return NotFound();

                _applicationServiceComment.Add(CommentDTO);
                return Ok("Resposta cadastrada com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/Comment/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] CommentDTO CommentDTO)
        {
            try
            {
                if (CommentDTO == null)
                    return NotFound();

                _applicationServiceComment.Update(CommentDTO);
                return Ok("Resposta atualizada com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete([FromBody] CommentDTO CommentDTO)
        {
            try
            {
                if (CommentDTO == null)
                    return NotFound();

                _applicationServiceComment.Remove(CommentDTO);
                return Ok("Resposta removida com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
