﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AnswerMe.Application.Interfaces;
using AnswerMe.Application.DTO.DTO;


namespace AnswerMe.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AnswerController : ControllerBase
    {
        private readonly IApplicationServiceAnswer _applicationServiceAnswer;

        public AnswerController(IApplicationServiceAnswer applicationServiceAnswer)
        {
            _applicationServiceAnswer = applicationServiceAnswer;
        }

        // GET: api/Answer
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceAnswer.GetAll());
        }

        // GET: api/Answer/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceAnswer.GetById(id));
        }

        // POST: api/Answer
        [HttpPost]
        public ActionResult Post([FromBody] AnswerDTO answerDTO)
        {
            try
            {
                if (answerDTO == null)
                    return NotFound();

                _applicationServiceAnswer.Add(answerDTO);
                return Ok("Resposta cadastrada com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/Answer/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] AnswerDTO answerDTO)
        {
            try
            {
                if (answerDTO == null)
                    return NotFound();

                _applicationServiceAnswer.Update(answerDTO);
                return Ok("Resposta atualizada com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete([FromBody] AnswerDTO answerDTO)
        {
            try
            {
                if (answerDTO == null)
                    return NotFound();

                _applicationServiceAnswer.Remove(answerDTO);
                return Ok("Resposta removida com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
