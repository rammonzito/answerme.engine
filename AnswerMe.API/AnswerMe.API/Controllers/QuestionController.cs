﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using AnswerMe.Application.Interfaces;
using AnswerMe.Application.DTO.DTO;

namespace AnswerMe.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class QuestionController : ControllerBase
    {
        private readonly IApplicationServiceQuestion _applicationServiceQuestion;

        public QuestionController(IApplicationServiceQuestion applicationServiceQuestion)
        {
            _applicationServiceQuestion = applicationServiceQuestion;
        }

        // GET: api/Question
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return Ok(_applicationServiceQuestion.GetAll());
        }

        // GET: api/Question/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return Ok(_applicationServiceQuestion.GetById(id));
        }

        // POST: api/Question
        [HttpPost]
        public ActionResult Post([FromBody] QuestionDTO QuestionDTO = null)
        {
            try
            {
                if (QuestionDTO == null)
                    return NotFound();

                _applicationServiceQuestion.Add(QuestionDTO);
                return Ok("Resposta cadastrada com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        // PUT: api/Question/5
        [HttpPut("{id}")]
        public ActionResult Put(int id, [FromBody] QuestionDTO QuestionDTO)
        {
            try
            {
                if (QuestionDTO == null)
                    return NotFound();

                _applicationServiceQuestion.Update(QuestionDTO);
                return Ok("Resposta atualizada com sucesso!");
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public ActionResult Delete([FromBody] QuestionDTO QuestionDTO)
        {
            try
            {
                if (QuestionDTO == null)
                    return NotFound();

                _applicationServiceQuestion.Remove(QuestionDTO);
                return Ok("Resposta removida com sucesso!");
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}
