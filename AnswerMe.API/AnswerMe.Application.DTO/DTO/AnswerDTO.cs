﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.DTO.DTO
{
    public class AnswerDTO : BaseDTO
    {
        public bool Correct { get; set; }
    }
}
