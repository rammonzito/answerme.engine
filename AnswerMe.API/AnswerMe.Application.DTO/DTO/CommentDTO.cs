﻿using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Application.DTO.DTO
{
    public class CommentDTO : BaseDTO
    {
        public int RelatedObjectId { get; set; }
    }
}
