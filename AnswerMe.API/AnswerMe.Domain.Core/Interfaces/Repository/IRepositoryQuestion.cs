﻿using AnswerMe.Domain.Models;

namespace AnswerMe.Domain.Core.Interfaces.Repository
{
    public interface IRepositoryQuestion : IRepositoryBase<Question>
    {
    }
}
