﻿using AnswerMe.Domain.Models;

namespace AnswerMe.Domain.Core.Interfaces.Repository
{
    public interface IRepositoryComment : IRepositoryBase<Comment>
    {
    }
}
