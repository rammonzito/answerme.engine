﻿using AnswerMe.Domain.Core.Interfaces;
using AnswerMe.Domain.Models;
using System;
using System.Collections.Generic;
using System.Text;

namespace AnswerMe.Domain.Core.Services
{
    public interface IServiceAnswer : IServiceBase<Answer>
    {
    }
}
